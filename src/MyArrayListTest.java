public class MyArrayListTest {
 void testMe(MyArrayList myArrayList, Results results) {
	 singleInsertion(myArrayList, results);
	 doubleInsertion(myArrayList, results);
	 insertionWithDuplicates(myArrayList, results);
	 deleteValue(myArrayList, results);
	 multipleDelete(myArrayList, results);
 }
 
 void singleInsertion(MyArrayList myArrayList, Results results) {
	 boolean flag = false;
	 myArrayList.insertSorted(78);
	 for(int i=0;i<myArrayList.siz;i++) {
		 if(myArrayList.intarray[i] == 78)
			  flag = true;
		 }
	 if(flag) {
		 results.storeNewResult("Test Single Insertion Passed" );
	 }
}
void doubleInsertion(MyArrayList myArrayList, Results results) {
	 boolean flag = false, flag1 = false;
	 myArrayList.insertSorted(78);
	 myArrayList.insertSorted(780);
	 for(int i=0;i<myArrayList.siz;i++) {
		 if(myArrayList.intarray[i] == 78)
			  flag = true;
		 }
	 for(int i=0;i<myArrayList.siz;i++) {
		 if(myArrayList.intarray[i] == 780)
			  flag1 = true;
		 }
	 if(flag && flag1) {
		 results.storeNewResult("Test Double Insertion Passed" );
	 }
	}
void insertionWithDuplicates(MyArrayList myArrayList, Results results) {
	 boolean flag = false;
	 int count = 0;
	 myArrayList.insertSorted(0);
	 myArrayList.insertSorted(0);
	 for(int i=0;i<myArrayList.siz;i++) {
		 if(myArrayList.intarray[i] == 0) {
			  flag = true;
		      count++;
		   }
	 }
	 System.out.println(flag+" "+count);
	 if(flag && count == 3) {
		 results.storeNewResult("Test insertionWithDuplicates Passed" );
	 }
	 else
		 results.storeNewResult("Test Failed");
}
void deleteValue(MyArrayList myArrayList, Results results) {
	 boolean flag = false;
	 myArrayList.removeValue(12);
	 for(int i=0;i<myArrayList.siz;i++) {
		 if(myArrayList.intarray[i] == 12)
			  flag = false;
			  else
				  flag = true;
		 }
	 if(flag) {
		 results.storeNewResult("Test delete Value Passed" );
	 }
	 else
		 results.storeNewResult("No Element Found To Delete");
}

void multipleDelete(MyArrayList myArrayList, Results results) {
	 boolean flag = false,flag1 = false;
	 myArrayList.removeValue(12);
	 myArrayList.removeValue(0);
	 for(int i=0;i<myArrayList.siz;i++) {
		 if(myArrayList.intarray[i] == 12)
			  flag = false;
			  else
				  flag = true;
		 }
	 for(int i=0;i<myArrayList.siz;i++) {
	 if(myArrayList.intarray[i] == 0)
		  flag1 = false;
		  else
			  flag1 = true;
	 }
	 if(flag && flag1) {
		 results.storeNewResult("Test multiple Delete Passed" );
	 }
	 else
		 results.storeNewResult("All Elements Not Found To Delete");
	 
}
void method6() {
	 
}
void method7() {
	 
}
void method8() {
	 
}
void method9() {
	 
}
void method10() {
	 
}

}

